package com.example.sqlite_android;

import java.util.ArrayList;

public class NhanVien {
    private int MaNV;
    private String TenNV,SoDT,DiaChi;


    public NhanVien(int maNV, String tenNV, String soDT, String diaChi) {
        MaNV = maNV;
        TenNV = tenNV;
        SoDT = soDT;
        DiaChi = diaChi;
    }

    public NhanVien() {
    }

    public int getMaNV() {
        return MaNV;
    }

    public void setMaNV(int maNV) {
        MaNV = maNV;
    }

    public String getTenNV() {
        return TenNV;
    }

    public void setTenNV(String tenNV) {
        TenNV = tenNV;
    }

    public String getSoDT() {
        return SoDT;
    }

    public void setSoDT(String soDT) {
        SoDT = soDT;
    }

    public String getDiaChi() {
        return DiaChi;
    }

    public void setDiaChi(String diaChi) {
        DiaChi = diaChi;
    }

    @Override
    public String toString() {
        return TenNV + "        " + SoDT + "         " + DiaChi;
    }
}
