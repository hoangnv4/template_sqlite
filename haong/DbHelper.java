package com.example.sqlite_android;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper {
    /**
     * Context la quyen truy cap,nghia la dinh nghia man nao co the truy cap contructor nay
     * */
    public DbHelper(Context context) {
        super(context,"Sqlite",null,4);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "Create table NhanVien(MaNV integer primary key autoincrement," + " TenNV text,SoDT text,DiaChi text)";
        db.execSQL(sql);
        sql = "Insert Into NhanVien Values (null, 'Nguyen Van A' , '0399074464','Thai Nguyen')";
        db.execSQL(sql);
        sql = "Insert Into NhanVien Values (null, 'Nguyen Van B' , '0399074464','Thai Nguyen')";
        db.execSQL(sql);
        sql = "Insert Into NhanVien Values (null, 'Nguyen Van C' , '0399074464','Thai Nguyen')";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("Drop Table if exists NhanVien");
        onCreate(db);
    }
}
