package com.example.sqlite_android;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Button btnThemMoi;
    ListView lv;
    ArrayAdapter adapter;
    ArrayList<NhanVien> dsNV = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnThemMoi = findViewById(R.id.btnThemMoi);
        lv = findViewById(R.id.lvNhanVien);
        dsNV = NhanVienDao.getAll(MainActivity.this); // CHUYEN MAN KHOI TAO
        adapter = new ArrayAdapter(MainActivity.this, android.R.layout.simple_list_item_1,dsNV); // chua hieu 2 thuoc tinh dau
        lv.setAdapter(adapter);

        btnThemMoi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDailogInsert();
            }
        });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showDailogUpdate(position);
            }
        });

    }

    private void showDailogInsert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this); //
        LayoutInflater layoutInflater = getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.dailog_them_nhan_vien,null);
        builder.setView(view);
        Dialog dialog = builder.create();
        dialog.show();

        EditText edten = view.findViewById(R.id.ten);
        EditText eddiachi = view.findViewById(R.id.diachi);
        EditText edsdt = view.findViewById(R.id.sdt);
        Button btnluu = view.findViewById(R.id.luuthemmoi);

        btnluu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              String ten = edten.getText().toString();
              String diachi = eddiachi.getText().toString();
              String sdt = edsdt.getText().toString();
              if(NhanVienDao.insert(MainActivity.this,ten,diachi,sdt)){
                  Toast.makeText(MainActivity.this, "Them moi thanh cong", Toast.LENGTH_SHORT).show();
                  dsNV.clear();
                  dsNV.addAll(NhanVienDao.getAll(MainActivity.this));
                  adapter.notifyDataSetChanged();
                  dialog.dismiss();
              }
              else  {
                  Toast.makeText(MainActivity.this, "Them khong thanh cong", Toast.LENGTH_SHORT).show();
              }
            }
        });

    }

    private void showDailogUpdate(int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this); //
        LayoutInflater layoutInflater = getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.dialog_update_nhan_vien,null);
        builder.setView(view);
        Dialog dialog = builder.create();
        dialog.show();

        EditText udten = view.findViewById(R.id.tenUD);
        EditText uddiachi = view.findViewById(R.id.diachiUD);
        EditText udsdt = view.findViewById(R.id.sdtUD);
        Button btnluucapnhat = view.findViewById(R.id.luucapnhat);
        Button btnxoa = view.findViewById(R.id.xoa);

        NhanVien nhanVien = dsNV.get(position);
        udten.setText(nhanVien.getTenNV());
        uddiachi.setText(nhanVien.getDiaChi());
        udsdt.setText(nhanVien.getSoDT());

        btnluucapnhat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nhanVien.setTenNV(udten.getText().toString());
                nhanVien.setDiaChi(uddiachi.getText().toString());
                nhanVien.setSoDT(udsdt.getText().toString());

                if(NhanVienDao.update(MainActivity.this,nhanVien)){
                    Toast.makeText(MainActivity.this, "Sua thanh cong", Toast.LENGTH_SHORT).show();
                    dsNV.clear();
                    dsNV.addAll(NhanVienDao.getAll(MainActivity.this));
                    adapter.notifyDataSetChanged();
                    dialog.dismiss();
                }
                else  {
                    Toast.makeText(MainActivity.this, "Sua khong thanh cong", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnxoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(NhanVienDao.delete(MainActivity.this,nhanVien.getMaNV())){
                    Toast.makeText(MainActivity.this, "Xoa thanh cong", Toast.LENGTH_SHORT).show();
                    dsNV.clear();
                    dsNV.addAll(NhanVienDao.getAll(MainActivity.this));
                    adapter.notifyDataSetChanged();
                    dialog.dismiss();
                }
                else  {
                    Toast.makeText(MainActivity.this, "Xoa khong thanh cong", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


}