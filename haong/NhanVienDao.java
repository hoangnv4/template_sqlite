package com.example.sqlite_android;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class NhanVienDao {
    public static ArrayList<NhanVien> getAll(Context context) {
        ArrayList<NhanVien> ds = new ArrayList<>();
        DbHelper helper = new DbHelper(context);
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cs = db.rawQuery("Select * from NhanVien",null);
        cs.moveToFirst();
        while (!cs.isAfterLast()) {
            int ma = cs.getInt(0);
            String ten = cs.getString(1);
            String sdt = cs.getString(2);
            String diachi = cs.getString(3);
            NhanVien nv = new NhanVien(ma,ten,sdt,diachi);
            ds.add(nv);
            cs.moveToNext();
        }
        cs.close();
        db.close();
        return ds;
    }

    public static Boolean insert(Context context,String tenNV, String soDT, String diaChi) {
        DbHelper helper = new DbHelper(context);
        SQLiteDatabase db = helper.getReadableDatabase();
        ContentValues values = new ContentValues();
        //TenNV text,SoDT text,DiaChi text
        values.put("TenNV",tenNV);
        values.put("SoDT",soDT);
        values.put("DiaChi",diaChi);
        long row = db.insert("NhanVien",null,values);
        return (row > 0);
    }

    public static Boolean update(Context context,NhanVien nv) {
        DbHelper helper = new DbHelper(context);
        SQLiteDatabase db = helper.getReadableDatabase();
        ContentValues values = new ContentValues();
        //TenNV text,SoDT text,DiaChi text
        values.put("TenNV",nv.getTenNV());
        values.put("SoDT",nv.getSoDT());
        values.put("DiaChi",nv.getDiaChi());
        int row = db.update("NhanVien",values,"MaNV = ?" ,new String[]{nv.getMaNV() + ""});
        return (row > 0);
    }


    public static Boolean delete(Context context , Integer maNV) {
        DbHelper helper = new DbHelper(context);
        SQLiteDatabase db = helper.getReadableDatabase();
        int row = db.delete("NhanVien","MaNV = ?",new String[]{maNV + ""});
        return (row > 0);
    }
}
